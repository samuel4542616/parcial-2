import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PokeePageRoutingModule } from './pokee-routing.module';

import { PokeePage } from './pokee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PokeePageRoutingModule
  ],
  declarations: [PokeePage]
})
export class PokeePageModule {}
