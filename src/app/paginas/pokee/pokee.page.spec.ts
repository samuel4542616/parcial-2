import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokeePage } from './pokee.page';

describe('PokeePage', () => {
  let component: PokeePage;
  let fixture: ComponentFixture<PokeePage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
