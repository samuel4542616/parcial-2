import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  allPokemon: any[] = [];
  displayedPokemon: any[] = [];

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.loadAllPokemon();
  }

  async loadAllPokemon() {
    try {
      this.allPokemon = await this.pokemonService.getAllPokemon();
      this.displayedPokemon = this.allPokemon;
    } catch (error) {
      console.error('Error loading Pokémon:', error);
    }
  }

  async searchPokemon(query: string) {
    if (!query.trim()) {
      this.displayedPokemon = this.allPokemon;
      return;
    }

    try {
      const data = await this.pokemonService.getPokemonByNameOrId(query.toLowerCase());
      this.displayedPokemon = [data];
    } catch (error) {
      console.error('Error searching Pokemon:', error);
      this.displayedPokemon = [];
    }
  }
}