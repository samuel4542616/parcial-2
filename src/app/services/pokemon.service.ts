import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor() { }

  async getPokemonByNameOrId(query: string) {
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${query}`); // Corrección aquí
      return response.data;
    } catch (error) {
      console.error('Error fetching Pokemon:', error);
      throw error;
    }
  }

  async getAllPokemon() {
    try {
      const response = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=1000');
      const pokemonList = await Promise.all(response.data.results.map(async (pokemon: any) => {
        const detailedResponse = await axios.get(pokemon.url);
        return detailedResponse.data;
      }));
      return pokemonList;
    } catch (error) {
      console.error('Error fetching all Pokemon:', error);
      throw error;
    }
  }
}