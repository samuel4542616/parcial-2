import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'SamuelSarmiento-GrupoE191-Examen2',
  webDir: 'www'
};

export default config;
